package com.sd365.permission.centre.pojo.query;

import com.sd365.common.core.common.pojo.vo.TenantBaseQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: zhengkongjin
 * @Date: 2020/12/11/ 0011 21:52
 * @Description:
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TenantQuery extends TenantBaseQuery {
    /**
     * 租户名
     */
    private String name;

    /**
     * 租户账号
     */
    private String account;
}
