/**
 * Copyright (C), 2022-2025, www.bosssof.com.cn
 * @FileName UserRoleDTO.java
 * @Author Administrator
 * @Date 2022-9-28  17:57
 * @Description TODO
 * History:
 * <author> Administrator
 * <time> 2022-9-28  17:57
 * <version> 1.0.0
 * <desc> 该文件定义了对角色分配多用户的DTO对象，对象应用在前端先后端请求以及其他服务调用用户中心服务传递该参数
 */
package com.sd365.permission.centre.pojo.dto;

import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.checkerframework.checker.units.qual.A;

import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * @Class UserRoleDTO
 * @Description 该类在角色管理界面分配业务的时候提交后端，对应数据将存储到basic_user_role表
 * @Author Administrator
 * @Date 2022-9-28  17:58
 * @version 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value="角色分配用户DTO")
public class UserRoleDTO extends TenantBaseDTO {
    /**
     * 角色id列表
     */
    @ApiModelProperty(value = "角色列表不为空")
    @NotNull(message = "角色列表不为空")
    private List<Long> roleIds;
    /**
     * 用户id列表
     */
    @ApiModelProperty(value = "用户id列表不为空")
    @NotNull(message = "用户id列表不为空")
    private List<Long> userIds;
}
