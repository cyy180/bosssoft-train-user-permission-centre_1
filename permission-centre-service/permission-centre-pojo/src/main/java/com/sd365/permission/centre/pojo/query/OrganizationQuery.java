package com.sd365.permission.centre.pojo.query;

import com.sd365.common.core.common.pojo.vo.BaseQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @className: OrganizationQuery
 * @description: 组织查询类
 * @author: 何浪
 * @date: 2020/12/11 17:04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrganizationQuery extends BaseQuery {
    /**
     * 名称
     */
    private String name;

    /**
     * 编号
     */
    private String code;

    /**
     * 负责人
     */
    private String master;

    /**
     * 手机号
     */
    private String tel;

    /**
     * 地址
     */
    private String address;
}
