package com.sd365.permission.centre.pojo.query;

import com.sd365.common.core.common.pojo.vo.TenantBaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@ApiModel(description ="子系统")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubSystemQuery extends TenantBaseQuery {

    /**
     * 子系统名
     */
    @ApiModelProperty(value="name")
    private String name;

    /**
     * 代码
     */
    @ApiModelProperty(value="code")
    private String code;

    /**
     * 图标
     */
    @ApiModelProperty(value="imageUrl")
    private String imageUrl;


    /**
     * 备注
     */
    @ApiModelProperty(value="remark")
    private String remark;


}
