package com.sd365.permission.centre.pojo.dto;

import com.sd365.common.core.common.pojo.dto.BaseDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;

/**
 * @author WangHaitang
 * @date 2022/07/161538
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "basic_message")
public class MessageDTO extends BaseDTO {

    @ApiModelProperty(value = "messageType消息类型")
    private String messageType;

    @ApiModelProperty(value = "messageContent消息内容")
    private String messageContent;

    @ApiModelProperty(value = "pushTime推送间隔")
    private Long pushTime;

    @ApiModelProperty(value = "pushPath推送渠道")
    private String pushPath;

    @ApiModelProperty(value = "companyId公司id")
    private Long companyId;

    @ApiModelProperty(value = "orgId组织")
    private Long orgId;

    @ApiModelProperty(value = "tenantId租户")
    private Long tenantId;

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public Long getPushTime() {
        return pushTime;
    }

    public void setPushTime(Long pushTime) {
        this.pushTime = pushTime;
    }

    public String getPushPath() {
        return pushPath;
    }

    public void setPushPath(String pushPath) {
        this.pushPath = pushPath;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }
}
