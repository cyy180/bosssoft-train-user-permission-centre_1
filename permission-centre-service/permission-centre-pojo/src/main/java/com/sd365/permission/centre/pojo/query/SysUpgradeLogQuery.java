package com.sd365.permission.centre.pojo.query;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.sd365.common.core.common.pojo.vo.TenantBaseQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

@ApiModel(description ="升级日志QUERY")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SysUpgradeLogQuery extends TenantBaseQuery {

    Long id;

    /**
     * 升级日志标题
     */
    @ApiModelProperty(value="title升级日志标题")
    private String title;
    /**
     * 升级日志内容
     */
    @ApiModelProperty(value="content升级日志内容")
    private String content;
    /**
     * 子系统id
     */
    @ApiModelProperty(value="子系统idsubSystemId")
    private Long subSystemId;
    /**
     * 上一版本
     */
    @ApiModelProperty(value="上一版本oldVersion")
    private String oldVersion;
    /**
     * 这一版本
     */
    @ApiModelProperty(value="这一版本newVersion")
    private String newVersion;
    /**
     * 更新时间
     */
    @NotNull
    @ApiModelProperty(value="更新时间upgradeTime")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss") //接收时间类型
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss") //返回时间类型
    private Date upgradeTime;


    @NotNull
    @ApiModelProperty(value="更新时间从fromUpgradeTime")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss") //接收时间类型
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss") //返回时间类型
    private Date fromUpgradeTime;

    @NotNull
    @ApiModelProperty(value="更新时间到toUpgradeTime")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss") //接收时间类型
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss") //返回时间类型
    private Date toUpgradeTime;

    /**
     * 更新备注
     */
    @ApiModelProperty(value="更新备注upgradeRemark")
    private String upgradeRemark;
    /**
     * 升级操作人员
     */
    @ApiModelProperty(value="升级操作人员upgradeOperator")
    private String upgradeOperator;

}
