package com.sd365.permission.centre.pojo.dto;

import com.sd365.common.core.common.pojo.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;


@ApiModel(value="com.sd365.permission.centre.entity.Administrative")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AdministrativeDTO extends BaseDTO {
    /**
     * 区域代码
     */
    @NotNull(message = "代码不能为空")
    @ApiModelProperty(value="code区域代码")
    private Long code;
    /**
     * 助记码
     */
    @NotNull(message = "助记码不能为空")
    @ApiModelProperty(value="mnemonicCode助记码")
    private String mnemonicCode;

    /**
     * 名称
     */
    @NotNull(message = "名称不能为空")
    @ApiModelProperty(value="name区域名称")
    private String name;

    /**
     * 区域类型
     */
    @NotNull(message = "类型不能为空")
    @ApiModelProperty(value="areaType类型")
    private String areaType;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;

    /**
     * 排序号
     */
    @NotNull(message = "排序号不能为空")
    @Min(value = 1)
    @ApiModelProperty(value="orderNum排序号")
    private Long orderNum;
    /**
     * 组织ID
     */
    @ApiModelProperty(value="orgId组织id")
    private Long orgId;

    /**
     * 租户ID
     */
    @ApiModelProperty(value="tenantId租户id")
    private Long tenantId;

    /**
     * 上级区域区域id
     */
    @ApiModelProperty(value="parentId上级区域id")
    private Long parentId;
    /**
     * 修改者
     */
    @ApiModelProperty(value="modifier修改者")
    private String modifier;
    /**
     * 创建者
     */
    @ApiModelProperty(value="creator创建者")
    private String creator;
    /**
     * 上级区域
     */
    private AdministrativeDTO upAdministrative;
}
