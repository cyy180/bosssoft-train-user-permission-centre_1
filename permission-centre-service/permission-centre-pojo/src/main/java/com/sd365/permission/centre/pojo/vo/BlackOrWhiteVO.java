package com.sd365.permission.centre.pojo.vo;

import com.sd365.common.core.common.pojo.vo.TenantBaseVO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@ApiModel(value="com.sd365.permission.centre.pojo.dto.BlackOrWhite")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlackOrWhiteVO extends TenantBaseVO {
    private UserVO userVO=new UserVO();
    private Long id;
    /**
     *
     */
    @ApiModelProperty(value="userId")
    private String userId;
    /**
     *
     */
    @ApiModelProperty(value="blackOrWhiteAccount")
    private String blackOrWhiteAccount;

    /**
     *
     */
    @ApiModelProperty(value="blackOrWhiteName")
    private String blackOrWhiteName;

    /**
     *
     */
    @ApiModelProperty(value="startTime")
    private Date startTime;

    /**
     *
     */
    @ApiModelProperty(value="endTime")
    private Date endTime;

    /**
     *
     */
    @ApiModelProperty(value="type")
    private String type;

    /**
     *
     */
    @ApiModelProperty(value="listSource")
    private String listSource;

    /**
     *
     */
    @ApiModelProperty(value="note")
    private String note;

    /**
     *
     */
    public String getBlackOrWhiteAccount() {
        return blackOrWhiteAccount;
    }

    /**
     *
     */
    public void setBlackOrWhiteAccount(String blackOrWhiteAccount) {
        this.blackOrWhiteAccount = blackOrWhiteAccount;
    }

    /**
     *
     */
    public String getBlackOrWhiteName() {
        return blackOrWhiteName;
    }

    /**
     *
     */
    public void setBlackOrWhiteName(String blackOrWhiteName) {
        this.blackOrWhiteName = blackOrWhiteName;
    }

    /**
     *
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     *
     */
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    /**
     *
     */
    public Date getEndTime() {
        return endTime;
    }

    /**
     *
     */
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    /**
     *
     */
    public String getType() {
        return type;
    }

    /**
     *
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     */
    public String getListSource() {
        return listSource;
    }

    /**
     *
     */
    public void setListSource(String listSource) {
        this.listSource = listSource;
    }

    /**
     *
     */
    public String getNote() {
        return note;
    }

    /**
     *
     */
    public void setNote(String note) {
        this.note = note;
    }
}