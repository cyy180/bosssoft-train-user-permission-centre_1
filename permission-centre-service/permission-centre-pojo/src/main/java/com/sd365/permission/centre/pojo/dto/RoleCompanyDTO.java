package com.sd365.permission.centre.pojo.dto;

import com.sd365.common.core.common.pojo.dto.TenantBaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @program: sd365-permission-centre
 * @description: RoleCompanyDTO类
 * @author: wujiandong
 * @create: 2022-08-29
 **/
@ApiModel(value = "角色公司管理")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleCompanyDTO extends TenantBaseDTO {

    /**
     * 角色ID
     */
    @ApiModelProperty(value = "roleIds待分配角色ID列表")
    @NotNull
    private List<Long> roleIds;

    /**
     * 角色id列表
     */
    @ApiModelProperty(value = "authCompanyIds授权公司ID列表")
    @NotNull
    private List<Long> authCompanyIds;
}
