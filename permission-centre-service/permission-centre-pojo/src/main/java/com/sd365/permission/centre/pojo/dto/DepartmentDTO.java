package com.sd365.permission.centre.pojo.dto;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DepartmentDTO extends TenantBaseEntity {
    private List<DepartmentDTO> departmentDTOS=null;
    private CompanyDTO companyDTO;
    /**
     * 部门名称
     */
    @ApiModelProperty(value="name部门名称")
    @NotNull(message = "部门名称不能为空")
    private String name;

    /**
     * 助记码
     */
    @ApiModelProperty(value="mnemonicCode助记码")
    private String mnemonicCode;

    /**
     * 部门编号
     */
    @ApiModelProperty(value="code部门编号")
    private String code;

    /**
     * 部门级别
     */
    @ApiModelProperty(value="level部门级别")
    private String level;

    /**
     * 上级部门
     */
    @ApiModelProperty(value="parentId上级部门")
    private Long parentId;

    /**
     * 负责人
     */
    @ApiModelProperty(value="master负责人")
    private String master;

    /**
     * 部门描述
     */
    @ApiModelProperty(value="descript部门描述")
    private String descript;

    /**
     * 隶属公司id
     */
    @ApiModelProperty(value="隶属公司id")
    private Long authCompanyId;
}