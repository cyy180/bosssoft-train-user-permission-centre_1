package com.sd365.permission.centre.pojo.query;


import com.sd365.common.core.common.pojo.vo.TenantBaseQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @ClassName UserQuery
 * @Description 用户的搜索
 * @Author yangshaoqi
 * @Date 2020/12/15 13:13
 * @Version 1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PositionQuery extends TenantBaseQuery {



    private Long id;//职位id
    private String name;//职位名称
    private String code;//职位编号



}
