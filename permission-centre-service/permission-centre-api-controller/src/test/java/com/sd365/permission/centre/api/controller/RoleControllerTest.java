/**
 * Copyright (C), 2022-2025, www.bosssof.com.cn
 * @FileName RoleControllerTest.java
 * @Author Administrator
 * @Date 2022-9-28  19:58
 * @Description 指定单元测试验收标准
 * History:
 * <author> Administrator
 * <time> 2022-9-28  19:58
 * <version> 1.0.0
 * <desc> 该文件主要对角色管理的 角色 增删改查 以及 角色分配资源和角色分配用户的测试用例进行要求
 * 验收程序通过 功能测试+单元测试的执行结果综合评定开发者的开发成果
 */
package com.sd365.permission.centre.api.controller;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
/**
 * @Class RoleControllerTest
 * @Description 针对控制器接口的单元测试类，选择了部分接口进行单元测试，开发人员必须按照模板
 * 完成单元测试，后续代码审查中重点审查单元测试是否能达到模板要求
 * @Author Administrator
 * @Date 2022-9-28  20:00
 * @version 1.0.0
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class RoleControllerTest {

    /**
     * @Description: 测试控制器的add方法
     * @Author: Administrator
     * @DATE: 2022-9-28  20:01
     * @param:
     */
    @Test
    void add() {
        // 测试步骤1
           // 测试必须输入项目完整的下角色添加是否成功
        // 测试步骤2
           // 测试必输项目不完整的情况下 是否触发异常添加是啊比
        // 测试步骤3
           // 测试加入重复的角色编号的角色对象 系统是否会报错触发异常
    }

    /**
     * @Description: 测试控制器的remove方法
     * @Author: Administrator
     * @DATE: 2022-9-28  20:17
     * @param:
     * @return:
     */
    @Test
    void remove() {
        // 测试步骤1
           // 测试传入存在角色id的删除是否成功 包括角色的关联资源和用户的关系都被清除
        // 测试步骤2
           // 测试传入不存在角色id的删除是否失败
        // 测试步骤3
          // 测试加入重复的角色编号的角色对象 系统是否会报错触发异常
        // 测试步骤4
           // 测试传入参数为空的时候是否触发异常
    }

    /**
     * @Description: 测试控制器 batchRemove方法
     * @Author: Administrator
     * @DATE: 2022-9-28  20:12
     * @param:
     * @return:
     */
    @Test
    void batchRemove() {
        // 测试步骤1
           // 测试删除一条角色数据的时候是否成功
        // 测试步骤2
            // 测试删除2条角色数据的时候是否成功
        // 测试步骤3
           // 测试参数数组为空的时候 是否触发异常
        // 测试步骤4
           // 测试参数数组不为空 但是id为空的时候是否异常
        // 测试步骤5
           // 传入数组中包含触发异常的测试数据 是否回滚事务，可以通过service的联合查询方法来验证
    }

    /**
     * @Description: 测试控制器 modify 方法
     * @Author: Administrator
     * @DATE: 2022-9-28  20:15
     * @param:
     * @return:
     */
    @Test
    void modify() {
        // 测试步骤1
             // 测试传入必输项完整的RoleDTO对象，成功修改
        // 测试步骤2
            // 测试传入必输项目不完整的对象，校验不通过触发异常
        // 测试步骤3
           // 测试传入编号重复的角色对象 修改不成功
    }

    /**
     * @Description: TODO
     * @Author: 测试控制器 assignResource方法
     * @DATE: 2022-9-28  20:24
     * @param:
     * @return:
     */
    @Test
    void assignResource() {
        //测试步骤1
            // 测试为一个新的角色分配 5条系统资源 分配成功
        //测试步骤2
            //　测试为一个已经分配资源的角色新增３条资源　分配成功　资源数量正确
        //测试步骤3
           // 测试为已经存在资源的角色 去除1条原有资源 加入2条新的资源，分配成功，资源正确
        //测试步骤4
           // 测试为２个角色同时分配 3条新资源，分配成功
        //测试步骤5
           //测试传入RoleResourceDTO 的角色列表为空 ，校验失败触发异常
        //测试步骤６
          // 测试传入RoleResourceDTO 的资源列表为空 ，分配成功角色所对应资源被清除
        // 测试步骤7 测试角色分配资源的事务性控制
          // 测试传入 RoleResourceDTO 其中包含一个可能触发异常的值，事务被回滚
    }

    @Test
    void assignUser() {
        //测试步骤1
        // 测试为一个新的角色分配 5条系统用户 分配成功
        //测试步骤2
        //　测试为一个已经分配资源的角色新增３条系统用户　分配成功　系统用户数量正确
        //测试步骤3
        // 测试为已经存在资源的角色 去除1条原有系统用户 加入2条新的系统用户，分配成功，系统用户正确
        //测试步骤4
        // 测试为２个角色同时分配 3条新系统用户，分配成功
        //测试步骤5
        //测试传入UserRoleDTO 的角色列表为空 ，校验失败触发异常
        //测试步骤６
        // 测试传入UserRoleDTO 的用户列表为空 ，分配成功角色所对应用户被清除
        // 测试步骤7 测试角色分配用户的事务性控制
        // 测试传入 RoleResourceDTO 其中包含一个可能触发异常的值，事务被回滚
    }

    @Test
    void haveRole() {
        // 测试步骤1
           // 传入一个存在的角色的编号 返回存在
        //测试步骤2
           // 传入一个不存在的角色的编号 返回不存在
        // 测试步骤3
           // 传入的编号为空 触发异常
    }

    /**
     * @Description: 针对控制器的commonQuery单元测试
     * @Author: Administrator
     * @DATE: 2022-9-28  20:47
     * @param:
     * @return:
     */
    @Test
    void commonQuery() {
        // 测试步骤1
            // 测试一个角色名模糊相等的 返回符合条件的记录 分页信息正确
        // 测试步骤2
           // 测试一个角色名字为空返回所有的角色 分页信息正确
        // 测试步骤3
            // 测试传入一个为名字为空 工号不好为空（工号模糊） 返回符合条件的记录
        // 测试步骤4
           // 测试传入一个 空的查询条件，返回所有的记录
    }
}