package com.sd365.permission.centre.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.code.CommonErrorCode;
import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.api.TenantApi;
import com.sd365.permission.centre.pojo.dto.DeleteTenantDTO;
import com.sd365.permission.centre.pojo.dto.TenantDTO;
import com.sd365.permission.centre.pojo.query.TenantQuery;
import com.sd365.permission.centre.pojo.vo.TenantVO;
import com.sd365.permission.centre.service.TenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @Auther: zhengkongjin
 * @Date: 2020/12/11/ 22:01
 * @Description:
 */
@RestController
public class TenantController extends AbstractController implements TenantApi {

    @Autowired
    private TenantService tenantService;

    @Override
    public Boolean add(@Valid TenantDTO tenantDTO) {
        return tenantService.add(tenantDTO);
    }

    @ApiLog
    @Override
    public List<TenantVO> commonQuery(TenantQuery tenantQuery) {
        if(null==tenantQuery){
            throw new BusinessException(CommonErrorCode.SYSTEM_CONTROLLER_ARGUMENT_NOT_VALID,new Exception("查询对象不允许为空"));
        }
        //执行查询
        List<TenantDTO> tenantDTOS= tenantService.commonQuery(tenantQuery);
        List<TenantVO> tenantVOS= BeanUtil.copyList(tenantDTOS, TenantVO.class);
        return tenantVOS;
    }

    @Override
    public Boolean remove(Long id, Long version) {
        return tenantService.remove(id,version);
    }

    @Override
    public Boolean modify(@Valid @RequestBody TenantDTO tenantDTO) {
        return tenantService.modify(tenantDTO);
    }

    @Override
    public Boolean batchRemove(@Valid DeleteTenantDTO[] deleteTenantDTOS) {
        return tenantService.batchDelete(deleteTenantDTOS);
    }

    @ApiLog
    @Override
    public TenantVO queryTenantById(@Valid @NotNull Long id) {
        //throw new BusinessException(CommonErrorCode.SYSTEM_SERVICE_ARGUMENT_NOT_VALID,new Exception("测试异常"));
        TenantDTO tenantDTO=tenantService.queryById(id);
        if(tenantDTO!=null){
            TenantVO tenantVO=BeanUtil.copy(tenantDTO,TenantVO.class);
            return tenantVO;
        }else{
            return null;
        }
    }

    @Override
    public TenantVO queryByName(String name) {
        TenantDTO tenantDTO=tenantService.queryByName(name);
        if(tenantDTO!=null){
            TenantVO stationVO=BeanUtil.copy(tenantDTO,TenantVO.class);
            return stationVO;
        }else{
            return null;
        }
    }
}
