package com.sd365.permission.centre.api.controller;

import com.sd365.common.log.api.annotation.ApiLog;
import com.sd365.permission.centre.api.LicensesApi;
import com.sd365.permission.centre.pojo.vo.LicensesVO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 读取配置文件类
 *
 * @author lenovo
 * @date 2022/07/142304
 **/
@RestController
public class LicensesController implements LicensesApi {

    @Value("${licenses.licensesDescription}")
    private String licensesDescription;

    @Value("${licenses.authorizedMachineCode}")
    private String authorizedMachineCode;

    @Value("${licenses.authorizedProductName}")
    private String authorizedProductName;

    @Value("${licenses.authorizedCompanyName}")
    private String authorizedCompanyName;

    @Value("${licenses.authorizedAccessAddress}")
    private String authorizedAccessAddress;

    @Value("${licenses.systemAccessAddress}")
    private String systemAccessAddress;

    @Value("${licenses.currentPlatformVersion}")
    private String currentPlatformVersion;

    @Value("${licenses.copyRight}")
    private String copyRight;

    @Override
    @ApiLog
    public List<LicensesVO> showInfo() {
        LicensesVO vo = new LicensesVO(licensesDescription, authorizedMachineCode, authorizedProductName, authorizedCompanyName, authorizedAccessAddress, systemAccessAddress, currentPlatformVersion, copyRight);
        List<LicensesVO> list = new ArrayList<>();
        list.add(vo);

        Iterator it = list.iterator();
        while(it.hasNext()){
            System.out.println(it.next());
        }
        return list;
    }
}
