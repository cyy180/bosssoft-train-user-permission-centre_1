package com.sd365.permission.centre.service;


import com.sd365.permission.centre.pojo.dto.SysUpgradeLogDTO;
import com.sd365.permission.centre.pojo.query.IdVersionQuery;
import com.sd365.permission.centre.pojo.query.SysUpgradeLogQuery;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

public interface SysUpgradeLogService {

    @Transactional
    List<SysUpgradeLogDTO> commonQuerySysUpgradeLog(SysUpgradeLogQuery sysUpgradeLogQuery);

    SysUpgradeLogDTO queryById(Long id);

    SysUpgradeLogDTO latestOne(Long tenantId, Long subSystemId);

    boolean add(@RequestBody @Valid SysUpgradeLogDTO sysUpgradeLogDTO);

    @Transactional
    Boolean remove(Long id, Long version);

    Boolean deleteBatch(List<IdVersionQuery> batchQuery);
}
