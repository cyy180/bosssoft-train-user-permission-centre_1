package com.sd365.permission.centre.service.exception;

import com.sd365.common.core.common.exception.code.IErrorCode;
/**
 * @Class UserCentreExceptionCode
 * @Description 根据系统的业务规则定义了业务类
 * @Author Administrator
 * @Date 2023-02-13  11:17
 * @version 1.0.0
 */
public enum UserCentreExceptionCode implements IErrorCode {

//    BUSINESS_TMS_MONITOR_ORGANIZATION_DB_EXCEPTION_CODE("210705", "机构管理模块数据库异常"),
//    BUSINESS_TMS_MONITOR_CAR_TRAIL_DB_EXCEPTION_CODE("210700", "轨迹模块数据库异常"),
//    BUSINESS_TMS_MONITOR_ELECTRON_FANCE_DB_EXCEPTION_CODE("210701", "电子围栏模块数据库异常"),
//    BUSINESS_TMS_MONITOR_MOTIFY_RECORD_DB_EXCEPTION_CODE("210702", "围栏设定通知消息模块数据库异常"),
//    BUSINESS_TMS_MONITOR_ORDER_STATISTICS_DB_EXCEPTION_CODE("210703", "监控订单统计模块数据库异常"),
//    BUSINESS_TMS_MONITOR_WAYBILL_STATE_DB_EXCEPTION_CODE("210704", "订单状态模块数据库异常"),
    /**
     * 客户操作存在增删改差和导出错误
     */
//    BUSINESS_BASEDATA_CUSTOMER_DB_EXCEPTION_CODE("210101", "客户管理模块数据库异常")，
//    BUSINESS_BASEDATA_CUSTOMER_PARAM_EXCEPTION_CODE("210102", "客户管理模块方法参数校验异常"),
//    BUSINESS_BASEDATA_ALLOCATION_DB_EXCEPTION_CODE("210103", "分拨点管理模块数据库异常"),
//    BUSINESS_BASEDATA_CARRIER_DB_EXCEPTION_CODE("210104", "承运商管理模块数据库异常"),
//    BUSINESS_BASEDATA_CUSTOMERADDRESSLIST_DB_EXCEPTION_CODE("210105", "客户地址管理模块数据库异常"),
//    BUSINESS_BASEDATA_CUSTOMER_FOUND_EXCEPTION_CODE("210106", "客户管理模块方法查到对象为空异常"),
//    BUSINESS_BASEDATA_ALLOCATION_FOUND_EXCEPTION_CODE("220012","分拨点管理模块方法查到对象为空异常"),
    /**
     *  字典操作异常"
     */
//    BUSINESS_BASEDATA_DICTIONARY_DB_EXCEPTION_CODE("210107","字典管理模块操作异常"),
//    BUSINESS_BASEDATA_DICTIONARY_BEAN_EXCEPTION_CODE("210108","字典管理模块操作异常"),
//    BUSINESS_BASEDATA_DICTIONARY_PARAM_EXCEPTION_CODE("210109","字典管理模块操作异常"),
//    BUSINESS_BASEDATA_DICTIONARY_TYPE_DB_EXCEPTION_CODE("210110", "字典类型模块数据库异常"),
    /**
     * 用户操作异常
     */
//    BUSINESS_BASEDATA_USER_DB_EXCEPTION_CODE("230501","用户管理模块数据库异常"),
//    BUSINESS_BASEDATA_USER_FOUND_EXCEPTION_CODE("230502","用户角色管理模块方法查到对象为空异常"),
//
//
//    BUSINESS_USER_CENTRE_ROLE_DB_EXCEPTION_CODE("210706", "角色模块数据库异常"),
//    RESOURCE_ASSIGN_FAILED("220804","资源分配失败，请检查该角色是否已经拥有该资源"),
//    BUSINESS_USER_CENTRE_ROLE_RESOURCE_DB_EXCEPTION_CODE("210707", "角色资源模块数据库异常"),
//    BUSINESS_PC_MONITOR_STATE_DB_EXCEPTION_CODE("210704", "数据库外键依赖异常"),
    /**
     * company操作
     */
//    BUSINESS_COMPANY_COMMONQUERY_EXCEPTION_CODE("310101", "公司查询异常"),
//    BUSINESS_COMPANY_REMOVE_EXCEPTION_CODE("310102", "公司批量删除异常"),
//    BUSINESS_COMPANY_REMOVE_BATCH_EXCEPTION_CODE("310103", "公司批量删除异常"),
    /**
     * 模块操作异常
     */
//    BUSINESS_BASEDATA_MODULE_DB_EXCEPTION_CODE("410001","模块管理操作异常"),

    /**
     * message操作异常
     */
    BUSINESS_BASEDATA_MESSAGE_DB_EXCEPTION_CODE("510001","消息管理操作异常");

    /**
     * 错误码
     */
    private String code;
    /**
     * 错误消息
     */
    private String message;

    UserCentreExceptionCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public String getCode() {
        return this.code;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public void setCode(String s) {
        this.code=code;
    }

    @Override
    public void setMessage(String s) {
        this.message=s;
    }
}
