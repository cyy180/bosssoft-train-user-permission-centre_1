/**
 * Copyright (C), -2025, www.bosssof.com.cn
 * @FileName RoleServiceImpl.java
 * @Author Administrator
 * @Date 2022-9-30  16:25
 * @Description 包含角色管理界面的功能实现
 * History:
 * <author> Administrator
 * <time> 2022-9-30  16:25
 * <version> 1.0.0
 * <desc> abel.zhan 优化代码结构 分配用户方法采用清除用户角色重新分配，优化角色的多资源分配方式 优化代码结构和效率改为批量插入
 */
package com.sd365.permission.centre.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.Page;
import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.IdGenerator;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.common.core.common.aop.MyPageInfo;
import com.sd365.common.core.common.constant.EntityConsts;
import com.sd365.common.core.common.exception.BusinessException;
import com.sd365.common.core.common.exception.DaoException;
import com.sd365.common.core.common.exception.code.BizErrorCode;
import com.sd365.common.core.common.exception.code.CommonErrorCode;
import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import com.sd365.common.core.common.service.AbstractBusinessService;
import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.common.util.BeanException;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.dao.mapper.*;
import com.sd365.permission.centre.entity.*;
import com.sd365.permission.centre.pojo.dto.*;
import com.sd365.permission.centre.pojo.query.RoleQuery;
import com.sd365.permission.centre.pojo.query.UserQuery;
import com.sd365.permission.centre.pojo.vo.RoleCompanyVO;
import com.sd365.permission.centre.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import tk.mybatis.mapper.entity.Example;

import javax.validation.Valid;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Class RoleServiceImpl
 * @Description able.zhan 2022-09-30 重构了 人员分配
 * @Author Administrator
 * @Date 2022-9-30  16:24
 * @version 1.0.0
 * TODO 负责模块开发的人员实现该业务类,请按界面功能测试用例以及Junit 规范完成功能
 */
@Service
@Slf4j
public class RoleServiceImpl /**extends AbstractBusinessService implements RoleService**/ {

}