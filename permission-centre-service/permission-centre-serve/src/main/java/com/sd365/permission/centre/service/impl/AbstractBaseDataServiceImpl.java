package com.sd365.permission.centre.service.impl;

import com.sd365.common.core.common.pojo.entity.BaseEntity;
import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import com.sd365.common.core.context.BaseContextHolder;

import java.util.Calendar;

public abstract class AbstractBaseDataServiceImpl {

    public void baseDataStuff4Add(Object o) {
        if (o != null) {
            if (o instanceof TenantBaseEntity) {
                TenantBaseEntity baseEntity = (TenantBaseEntity) o;
                baseEntity.setCreator(BaseContextHolder.getLoginUserName());
                baseEntity.setCreatedBy(BaseContextHolder.getLoginUserId());
                baseEntity.setCreatedTime(Calendar.getInstance().getTime());
                baseEntity.setModifier(BaseContextHolder.getLoginUserName());
                baseEntity.setUpdatedBy(BaseContextHolder.getLoginUserId());
                baseEntity.setUpdatedTime(Calendar.getInstance().getTime());
            } else if (o instanceof BaseEntity) {
                BaseEntity baseEntity = (BaseEntity) o;
                baseEntity.setCreatedBy(BaseContextHolder.getLoginUserId());
                baseEntity.setCreatedTime(Calendar.getInstance().getTime());
                baseEntity.setUpdatedBy(BaseContextHolder.getLoginUserId());
                baseEntity.setUpdatedTime(Calendar.getInstance().getTime());
            }
        }
    }

    /**
     * TenantBaseEntity baseEntity = (TenantBaseEntity) o;
     * baseEntity.setModifier(BaseContextHolder.getLoginUserName());
     * baseEntity.setUpdatedBy(BaseContextHolder.getLoginUserId());
     * baseEntity.setUpdatedTime(Calendar.getInstance().getTime());
     *
     * @param o
     */
    public void baseDataStuff4Updated(Object o) {

        if (o instanceof BaseEntity) {
            TenantBaseEntity baseEntity = (TenantBaseEntity) o;
            baseEntity.setModifier(BaseContextHolder.getLoginUserName());
            baseEntity.setUpdatedBy(BaseContextHolder.getLoginUserId());
            baseEntity.setUpdatedTime(Calendar.getInstance().getTime());
            baseEntity.setCreatedBy(BaseContextHolder.getLoginUserId());
            baseEntity.setCreatedTime(Calendar.getInstance().getTime());
            baseEntity.setUpdatedBy(BaseContextHolder.getLoginUserId());
            baseEntity.setUpdatedTime(Calendar.getInstance().getTime());
        }
    }


    public void baseDataStuffCommonPros(Object o) {

        if (o != null) {
            if (o instanceof TenantBaseEntity) {
                TenantBaseEntity baseEntity = (TenantBaseEntity) o;
                baseEntity.setTenantId(BaseContextHolder.getTanentId());
                baseEntity.setCompanyId(BaseContextHolder.getCompanyId());
                baseEntity.setOrgId(BaseContextHolder.getOrgId());
            } else if (o instanceof BaseEntity) {
                BaseEntity baseEntity = (BaseEntity) o;
                baseEntity.setCreatedBy(BaseContextHolder.getLoginUserId());
                baseEntity.setCreatedTime(Calendar.getInstance().getTime());
                baseEntity.setUpdatedBy(BaseContextHolder.getLoginUserId());
                baseEntity.setUpdatedTime(Calendar.getInstance().getTime());
            }
        }
    }

}
