//package com.sd365.permission.centre.service.impl;
//
//import com.sd365.permission.centre.entity.Resource;
//import com.sd365.permission.centre.pojo.vo.ResourceVO;
//import com.sd365.permission.centre.service.MenuService;
//
//import java.util.LinkedList;
//import java.util.List;
//
//public class Test {
//    public static void main(String[] args) {
//        MenuService menuService = new MenuServiceImpl();
//        List<Resource> resourceList = new LinkedList<>();
//
//        Resource resource1 = new Resource();
//        resource1.setId(1L);
//        resource1.setName("父结点");
//        resource1.setParentId(-1L);
//
//        Resource resource2 = new Resource();
//        resource2.setId(2L);
//        resource2.setName("子结点1");
//        resource2.setParentId(1L);
//
//        Resource resource3 = new Resource();
//        resource3.setId(3L);
//        resource3.setName("子结点2");
//        resource3.setParentId(1L);
//
//        Resource resource4 = new Resource();
//        resource4.setId(4L);
//        resource4.setName("孙子结点1");
//        resource4.setParentId(2L);
//
//        Resource resource5 = new Resource();
//        resource5.setId(5L);
//        resource5.setName("孙子结点2");
//        resource5.setParentId(3L);
//
//        resourceList.add(resource1);
//        resourceList.add(resource2);
//        resourceList.add(resource3);
//        resourceList.add(resource4);
//        resourceList.add(resource5);
//
//        ResourceVO resourceVO = menuService.buildMenu(resourceList);
//        System.out.println(resourceVO);
//        // 父节点
//        System.out.println(resourceVO.getName());
//        // 父节点的子孙结点
//        System.out.println(resourceVO.getResourceVOs().get(0).getName());
//        System.out.println(resourceVO.getResourceVOs().get(0).getResourceVOs().get(0).getName());
//
//
//        System.out.println(resourceVO.getResourceVOs().get(1).getName());
//        System.out.println(resourceVO.getResourceVOs().get(1).getResourceVOs().get(0).getName());
//
//    }
//}
