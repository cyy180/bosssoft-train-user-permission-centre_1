package com.sd365.permission.centre.service;


import com.sd365.permission.centre.pojo.dto.DeletePositionDTO;
import com.sd365.permission.centre.pojo.dto.PositionDTO;
import com.sd365.permission.centre.pojo.query.PositionQuery;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

/**
 * @author zwy
 * @version 1.0.0
 * @class UserService
 * @classdesc 维护职位信息的服务接口
 * @date 2020-12-14  21点28分
 * @see
 * @since
 */
public interface PositionService {

    /**
     * @param: 职位DTO
     * @return: 成功则true
     * @see
     * @since
     */
    @Transactional
    Boolean add(@RequestBody @Valid PositionDTO postionDTO);


    /**
     * @param: id与版本
     * @return: 成功则true
     * @see
     * @since
     */
    @Transactional
    Boolean remove(Long id, Long version);


    /**
     * @param: 职位DTO
     * @return: 成功则true
     * @see
     * @since
     */
    @Transactional
    Boolean modify(PositionDTO postionDTO);


    /**
     * @param: 职位搜索条件
     * @return: 职位列表
     * @see
     * @since
     */

    List<PositionDTO> commonQuery(PositionQuery postionQuery);

    /**
     * @param: 职位id
     * @return: user
     * @see
     * @since
     */
    PositionDTO queryById(Long id);

    /**
     * @param:
     * @return:
     * @see
     * @since
     */


    /**
     *  批量更新职位记录
     * @param PositionDTOS
     * @return
     */
    @Transactional
    Boolean batchUpdate(PositionDTO[] postionDTOS);


    /**
     * 批量删除职位记录
     * @param PositionDTOS
     * @return
     */
    @Transactional
    Boolean batchDelete(DeletePositionDTO[] deletePositionDTOS);


}
