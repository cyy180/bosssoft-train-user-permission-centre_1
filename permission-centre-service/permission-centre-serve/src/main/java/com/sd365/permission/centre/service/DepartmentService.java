package com.sd365.permission.centre.service;

import com.sd365.permission.centre.entity.Department;
import com.sd365.permission.centre.pojo.dto.DepartmentDTO;
import com.sd365.permission.centre.pojo.query.DepartmentQuery;
import com.sd365.permission.centre.pojo.query.IdVersionQuery;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import java.util.List;

/**
 * @Author jxd
 * @Date 2020/12/12  1:00 下午
 * @Version 1.0
 * @Write For OrganizationService
 * @Email waniiu@126.com
 */
public interface DepartmentService {

    List<Department> commonQueryDepartment(DepartmentQuery departmentQuery);

    /**
     * @param:
     * @return:
     * @see
     * @since
     */
    @Transactional
    Boolean modify(DepartmentDTO departmentDTO);

    /**
     * 单行起停用操作
     *
     * @param departmentDTO
     * @return
     * @author Yan Huazhi
     * @date 2020/12/18 9:47
     * @version 0.0.1
     */
    Boolean batchModify(DepartmentDTO departmentDTO);

    /**
     * @param: 客户订单DTO
     * @return: 成功则true
     * @see
     * @since
     */
    Boolean add(@RequestBody @Valid DepartmentDTO departmentDTO);


    /**
     * @param:
     * @return:
     * @see
     * @since
     */
    @Transactional
    Boolean remove(Long id, Long version);

    /**
     * @param:
     * @return:
     * @see
     * @since
     */
    List<DepartmentDTO> commonQuery(DepartmentQuery departmentQuery);


    DepartmentDTO queryById(Long id);

    DepartmentDTO copy(Long id);

    Boolean deleteBatch(List<IdVersionQuery> batchQuery);

    /**
     * 构建部门树
     *
     * @param departmentQuery
     * @return
     */
    List<DepartmentDTO> builderTree(DepartmentQuery departmentQuery);

    /**
     * 递归查询子节点
     * @param list
     * @param departmentDTO
     */
    void builderNode(List<DepartmentDTO> list, DepartmentDTO departmentDTO);

}
