package com.sd365.permission.centre.service.impl;
import com.sd365.permission.centre.dao.mapper.AuditMapper;
import com.sd365.permission.centre.service.AuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sd365.common.util.BeanUtil;
import com.sd365.permission.centre.dao.mapper.AuditMapper;
import com.sd365.permission.centre.entity.Audit;
import com.sd365.permission.centre.pojo.dto.AuditDTO;
import com.sd365.permission.centre.pojo.query.AuditQuery;
import com.sd365.permission.centre.service.AuditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
/**
 * @Class AuditServiceImpl
 * @Description 审计服务类，系统shecudle定时生成审计记录，该类查询审计记录
 * @Author Administrator
 * @Date 2023-02-20  19:58
 * @version 1.0.0
 */
@Service
public class AuditServiceImpl implements AuditService {
    /**
     * 审核表操作mapper
     */
    @Autowired
    private AuditMapper auditMapper;
    @Override
    public List<Audit> commonQuery(AuditQuery auditQuery) {
            return  auditMapper.commonQuery(auditQuery);
    }
    @Override
    public AuditDTO queryById(Long id) {
            Audit audit = auditMapper.selectById(id);
            return audit!=null ? BeanUtil.copy(audit, AuditDTO.class) : new AuditDTO();
    }

    /**
     * 通过是否使用简单密码查询
     * @param isusp
     * @return
     */
    @Override
    public List<AuditDTO> selectByUseSimplePwd(Byte isusp) {
            List<Audit> audits = auditMapper.selectByUseSimplePwd(isusp);
            return !CollectionUtils.isEmpty(audits) ? BeanUtil.copyList(audits, AuditDTO.class): new ArrayList<AuditDTO>();
    }

    /**
     * 通过是否长期未更新密码查询
     * @param isnup
     * @return
     */
    @Override
    public List<AuditDTO> selectByNoUpdatePwd(Byte isnup) {
            List<Audit> audits = auditMapper.selectByNoUpdatePwd(isnup);
           return  !CollectionUtils.isEmpty(audits) ? BeanUtil.copyList(audits, AuditDTO.class):new ArrayList<AuditDTO>();
    }

    /**
     * 通过是否长期未登录查询
     * @param islnl
     * @return
     */
    @Override
    public List<AuditDTO> selectByLongNoLogin(Byte islnl) {
            List<Audit> audits = auditMapper.selectByLongNoLogin(islnl);
            return CollectionUtils.isEmpty(audits) ? BeanUtil.copyList(audits, AuditDTO.class):new ArrayList<AuditDTO>();

    }
}
