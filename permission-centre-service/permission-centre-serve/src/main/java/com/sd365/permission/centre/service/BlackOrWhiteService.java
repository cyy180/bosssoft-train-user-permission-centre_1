package com.sd365.permission.centre.service;

import com.sd365.permission.centre.pojo.dto.BlackOrWhiteDTO;
import com.sd365.permission.centre.pojo.query.BlackOrWhiteQuery;
import com.sd365.permission.centre.pojo.vo.BlackOrWhiteVO;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;
@Validated
@CacheConfig(cacheNames = "BlackOrWhiteService",cacheManager = "BlackOrWhiteService")
public interface BlackOrWhiteService {
    /**
     *  依据条件查询黑白名单对象
     * @param blackOrWhiteQuery
     * @return 黑名名单列表
     */
    List<BlackOrWhiteVO> commonQuery(@NotNull BlackOrWhiteQuery blackOrWhiteQuery);

    /**
     *  增加黑白名单同时加入缓存
     * @param blackOrWhiteDTO
     * @return
     */
    @CachePut(key = "#p0.blackOrWhiteAccount")
    BlackOrWhiteVO add(@Valid  BlackOrWhiteDTO blackOrWhiteDTO);
    /**
     *  删除指定的黑名名单记录
     * @param id 记录id
     * @param version 版本
     * @return
     */
    Boolean remove(@NotNull Long id, @NotNull Long version);
    /**
     *  批量移除黑名名单
     * @param blackOrWhiteDTOS
     * @return
     */
    @Transactional
    Boolean batchRemove(@NotEmpty BlackOrWhiteDTO[] blackOrWhiteDTOS);

    /**
     * 借助该方法删除key
     * @param account
     * @return
     */
    @CacheEvict(key = "#p0")
    public Boolean removeCache(@NotNull String account) ;

    /**
     *  修改黑白名单记录
     * @param blackOrWhiteDTO
     * @return
     */
    @CachePut(key = "#p0.blackOrWhiteAccount")
    Boolean modify(@Valid  BlackOrWhiteDTO blackOrWhiteDTO);
    /**
     *  批量更新黑白名单
     * @param blackOrWhiteDTO
     * @return true成功 false失败
     */
    @Transactional
    Boolean batchModify(@NotEmpty  BlackOrWhiteDTO[] blackOrWhiteDTO);

    /**
     *  查询指定的黑白名单
     * @param id  指定黑白名单id
     * @return 返回黑白名单对象
     */
    @Cacheable(key = "#p0")
    BlackOrWhiteDTO queryById(@NotNull Long id);

    /**
     *  未知方法需要梳理
     * @param query
     * @return 黑白名单对象
     */
    @Cacheable(key = "#p0.id")
    BlackOrWhiteVO queryTest(BlackOrWhiteQuery query);
    /**
     * 批量插入黑名名单对象
     * @param
     * @return
     */
    Boolean batchAdd(@Valid  @RequestBody List<BlackOrWhiteDTO> blackOrWhiteDTOList);
}
