/**
 * Copyright (C), -2025, www.bosssof.com.cn
 * @FileName LoginService.java
 * @Author Administrator
 * @Date 2022-10-12  17:06
 * @Description abel.zhan 重构了部分方法增加了注释
 * History:
 * <author> Administrator
 * <time> 2022-10-12  17:06
 * <version> 1.0.0
 * <desc> abel.zhan 2022-10-12 重构 service 和 congroller
 * <br>具体描述:当前版本的问题在于 service设计的时候功能不够内聚导致成为一个贫血的service
 * <br>而使得LoginController的逻辑过去，因此对该类结果做重构，新增 auth 方法 整合相关调用
 */
package com.sd365.permission.centre.service;

import com.sd365.permission.centre.entity.Role;
import com.sd365.permission.centre.entity.User;
import com.sd365.permission.centre.pojo.dto.UserDTO;
import com.sd365.permission.centre.pojo.vo.ResourceVO;
import lombok.Data;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @interface LoginService
 * @Description 实现登录功能
 * @Author JiaoBingjie
 * @Date 2020/12/11 14:58
 * @version 1.0.0
 */
public interface LoginService {
    /**
     * 错误的租户
     */
     int LOGIN_VERIFY_CODE_TENANT_ERR=0;
    /**
     * 认证成功 账号 密码 租户一致
     */
    int LOGIN_VERIFY_CODE_SUCCESS=1;
    /**
     *  账号或者密码错误
     */
    int LOGIN_VERIFY_CODE_CODE_OR_PASSWORD_ERR=2;

    @Data
    public class LoginUserDTO{
        /**
         * 带回认证的错误码
         */
        private int resultCode;
        private Long id;
        private Long tenantId;
        private Long orgId;
        private Long companyId;
        private String name;
        private String tel;
        private List<Long> roleIds;

    }


    /**
     * @decription 验证登录用户状态：账号(code)和租户(account)不匹配，即 "该账户不存在"，返回0
     *                              账号和租户匹配，但密码错误，返回2
     *                              账号和租户存在数据库且匹配，以及密码正确，则登录成功，返回1
     * @param code, password, account
     * @return 0:账号错误  1:登录成功  2:密码错误
     */
    int verification(String code, String password, String account);

    /**
     * @Description: 存储是返回一个用户
     * @Author: Administrator
     * @DATE: 2022-10-12  17:02
     * @param:  工号和租户账号
     * @return:  返回用户 如果没找到这返回 null
     */
    public User getUserInfo(String code, String account);

    /**
     * @Description: TODO
     * @Author: Administrator
     * @DATE: 2022-10-12  17:03
     * @param:  用户id
     * @return:  取得用户的所有的角色 如果没有授权返回 null
     */
    List<Role> getRolesByUserId(Long userId);

    /**
     * 取得角色对应的资源
     *  2022-10-12 重构了该方法增加注释已经保证不返回空对象
     * @param roles  当前用户的所有的角色
     * @return 所拥有的资源 如果角色没有资源方法返回 null
     */
    List<ResourceVO> getResourceVO(List<Role> roles);

    /**
     * 用户登陆成功修改用户状态
     * @param userId,status
     * @return status  1 正常用户 0 注销用户 2 锁定用户 3 在线用户
     */
    Boolean updateUserOnlineStatus(Long userId,byte status);


    /**
     * @Description: 新增 auth方法 内部调用 认证和更新用户状态
     * @Author: abel.zhan
     * @DATE: 2022-10-12  17:59
     * @param: 为了兼容 原来的verification 方法调用而采用相同的参数
     * @return: LoginUserDTO.resultCode=0 和 2 代表失败，1 代表成功 成功情况下包含其他信息
     */
    LoginUserDTO auth(String code, String password, String account);
}
