package com.sd365.permission.centre.entity;

import com.sd365.common.core.common.pojo.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import java.util.Date;

@ApiModel(value="com.sd365.permission.centre.entity.Audit")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "basic_audit")
public class Audit extends BaseEntity {
    /**
     * 工号
     */
    private String code;
    /**
     * 昵称
     */
    private String name;
    /**
     * 部门id
     */
    private Long departmentId;
    /**
     *使用简单密码
     */
    private Byte useSimplePwd;
    /**
     * 定期未修改密码
     */
    private Byte noUpdatePwd;
    /**
     * 长期未登录
     */
    private Byte longNoLogin;
    /**
     * 审计结果
     */
    private String result;
    /**
     * 密码修改时间
     */
    private Date updatePwd;
    /**
     * 最后登录时间
     */
    private Date endLogin;
    /**
     * 修改者
     */
    private String modifier;
    /**
     * 创建者
     */
    private String creator;
}
