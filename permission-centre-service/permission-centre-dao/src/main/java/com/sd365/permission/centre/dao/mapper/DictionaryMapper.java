package com.sd365.permission.centre.dao.mapper;

import com.sd365.permission.centre.entity.Dictionary;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * @Class DictionaryMapper
 * @Description  mybatis 逆向生成的针对dicationary表的增删改查接口，也
 * 扩展了 commonQuery,selectByName,updateDictionary 几个方法
 * @Author Administrator
 * @Date 2023-02-22  14:29
 * @version 1.0.0
 */
public interface DictionaryMapper extends Mapper<Dictionary> {
    /**
     * 每个Mapper都可以定义属于自己通用查询
     * @param dictionary 查询对象一般是entity
     * @return 查询到的列表
     */
    List<Dictionary> commonQuery(Dictionary dictionary);

    /**
     *  做关联查询
     * @param id
     * @return
     */
    Dictionary selectById(Long id);

    /**
     *  做关联查询
     * @param id
     * @return
     */
    List<Dictionary> selectByTypeId(Long id);

    /**
     * @Description:  通过字典名查询
     * @Param:  String
     * @return:  List<Dictionary>
     * @Author: yuye
     * @Date: 2020/12/3
     */
    List<Dictionary> selectByName(String name);

    /**
     * 更新字典
     * @param dictionary 字典对象
     * @return 返回影响的行数
     */
    int updateDictionary(Dictionary dictionary);
}