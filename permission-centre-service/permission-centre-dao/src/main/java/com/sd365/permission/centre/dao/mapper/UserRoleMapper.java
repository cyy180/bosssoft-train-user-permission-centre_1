package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.UserRole;

import java.util.List;

public interface UserRoleMapper extends CommonMapper<UserRole> {
    List<UserRole> selectByUserId(Long id);

    Integer deleteAllByUserID(Long id);
    int haveUserRole(UserRole userRole);
    UserRole selectUserRoleByUseridTenantid(Long userId, Long tenantId);

}