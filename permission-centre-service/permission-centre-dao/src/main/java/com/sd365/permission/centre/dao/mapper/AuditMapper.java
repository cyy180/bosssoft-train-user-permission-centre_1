package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Audit;
import com.sd365.permission.centre.pojo.query.AuditQuery;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AuditMapper extends CommonMapper<Audit> {
    /**
     * 查询当前全部用户审计
     * @param auditquery
     * @return
     */
    List<Audit> commonQuery(AuditQuery auditquery);
    Audit selectById(@Param("id") Long id);
    List<Audit> selectByUseSimplePwd(@Param("isusp") Byte isusp);
    List<Audit> selectByNoUpdatePwd(@Param("isnup") Byte isnup);
    List<Audit> selectByLongNoLogin(@Param("islnl") Byte islnl);
}
