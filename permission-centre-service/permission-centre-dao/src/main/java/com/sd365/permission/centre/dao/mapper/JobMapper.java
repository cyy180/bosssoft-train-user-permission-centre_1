package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.JobManage;
import com.sd365.permission.centre.pojo.query.JobQuery;

import java.util.List;


public interface JobMapper extends CommonMapper<JobManage> {

    List<JobManage> commonQueryJob(JobQuery jobQuery);

    void pauseJobStatus(Long id);

    void resumeJobStatus(Long id);

    int insertJob(JobManage job);

    int updateJob(JobManage job);

}
