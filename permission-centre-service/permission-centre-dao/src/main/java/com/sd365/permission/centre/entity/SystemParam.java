package com.sd365.permission.centre.entity;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;

import javax.persistence.Table;

@ApiModel(value="com.sd365.permission.centre.entity.SystemParam")
@Table(name = "basic_system_param")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SystemParam extends TenantBaseEntity {
    /**
     * 字典类型_ID
     */
    @ApiModelProperty(value="paramType字典类型_ID")
    private Long paramType;

    /**
     * 参数项
     */
    @ApiModelProperty(value="param参数项")
    private String param;

    /**
     * 参数值
     */
    @ApiModelProperty(value="value参数值")
    private String value;

    /**
     * 公司属性
     */
    @org.springframework.data.annotation.Transient
    @ApiModelProperty(value="公司属性")
    private Company company;

    /**
     * 机构属性
     */
    @org.springframework.data.annotation.Transient
    @ApiModelProperty(value="机构属性")
    private Organization organization;

    /**
     * 字典类型属性
     */
    @Transient
    @ApiModelProperty(value="字典类型属性")
    private DictionaryType dictionaryCategory;

}
