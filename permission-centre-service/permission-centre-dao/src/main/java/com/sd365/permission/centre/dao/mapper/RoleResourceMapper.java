package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Node;
import com.sd365.permission.centre.entity.Resource;
import com.sd365.permission.centre.entity.RoleResource;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RoleResourceMapper extends CommonMapper<RoleResource> {

    /**
     * 根据id 使用多表链接查询查找roleResource对象
     * @param id role 的 id
     * @return 带有 公司 机构  客户等字段的 roleResource 对象
     */
    RoleResource selectById(Long id);

    /**
     * 每个Mapper都可以定义属于自己通用查询
     * @param roleResource 查询对象一般是entity
     * @return 查询到的列表
     */
    List<RoleResource> commonQuery(RoleResource roleResource);

    /**
     * 根据角色ID查询分配的资源
     * @param id
     * @return
     */
    List<Node> queryResourceByRoleId(@Param("id") Long id);

    /**
     * 判断数据是否已经存在
     * @param roleResource
     * @return
     */
    int haveRoleResource(RoleResource roleResource);

    int insertRoleResource(RoleResource roleResource);

    public Resource getResourceByRoleIds(List<Long> roleIds);

    void deleteByRoleId(Long roleId);
}
