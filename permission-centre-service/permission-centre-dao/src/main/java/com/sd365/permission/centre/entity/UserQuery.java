package com.sd365.permission.centre.entity;

import com.sd365.common.core.common.pojo.vo.TenantBaseQuery;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @ClassName UserQuery
 * @Description 用户的搜索
 * @Author yangshaoqi
 * @Date 2020/12/15 13:13
 * @Version 1.0
 **/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserQuery extends TenantBaseQuery {
    /**
     * 名字
     */
    private String name;
    /**
     * 工号
     */
    private String code;
    /**
     * 电话
     */
    private String tel;
    /**
     * 角色
     */
    private Long roleId;
}
