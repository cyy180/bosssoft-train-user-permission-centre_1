package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Module;
import com.sd365.permission.centre.pojo.query.ModuleQuery;

import java.util.List;

public interface ModuleMapper extends CommonMapper<Module> {

    /**
     * 通用查询带分页
     * @param moduleQuery
     * @return
     */
    List<Module> commonQuery(ModuleQuery moduleQuery);

//    Module insertModule(Module module);

    int updateModule(Module module);

    Module selectById(Long id);
}
