package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.SysUpgradeLog;
import com.sd365.permission.centre.pojo.query.SysUpgradeLogQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SysUpgradeLogMapper extends CommonMapper<SysUpgradeLog> {


    /**
     *
     * @param sysUpgradeLogQuery 升级日志查询对象
     * @return 查询到的 SysUpgradeLog 列表
     */
    List<SysUpgradeLog> commonSysUpgradeLogQuery(SysUpgradeLogQuery sysUpgradeLogQuery);

    SysUpgradeLog latestOne(SysUpgradeLog sysUpgradeLog);

    SysUpgradeLog selectById(Long id);
}
