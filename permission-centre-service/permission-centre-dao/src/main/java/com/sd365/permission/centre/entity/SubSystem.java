package com.sd365.permission.centre.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import java.util.Date;

@ApiModel(value="com.sd365.permission.centre.entity.SubSystem")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "basic_sub_system")
public class SubSystem extends TenantBaseEntity {
    /**
     * 子系统名
     */
    @ApiModelProperty(value="name子系统名")
    private String name;

    /**
     * 子系统代码
     */
    @ApiModelProperty(value="code子系统代码")
    private String code;

    /**
     * 子系统图标
     */
    @ApiModelProperty(value="imageUrl子系统图标")
    private String imageUrl;

    /**
     * 链接
     */
    @ApiModelProperty(value="link链接")
    private String link;

    /**
     * 备注
     */
    @ApiModelProperty(value="remark备注")
    private String remark;

    @ApiModelProperty(value="createdTime")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    /**
     *
     */
    @ApiModelProperty(value="updatedTime")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

    /**
     * 子系统名
     */
    public String getName() {
        return name;
    }

    /**
     * 子系统名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 子系统代码
     */
    public String getCode() {
        return code;
    }

    /**
     * 子系统代码
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 子系统图标
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * 子系统图标
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * 链接
     */
    public String getLink() {
        return link;
    }

    /**
     * 链接
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * 备注
     */
    public String getRemark() {
        return remark;
    }

    /**
     * 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 公司
     */
    @ApiModelProperty(value="company")
    private Company company;

    /**
     * 机构
     */
    @ApiModelProperty(value="company")
    private Organization organization;

    /**
     * 租户
     */
    @ApiModelProperty(value="company")
    private Tenant tenant;

}