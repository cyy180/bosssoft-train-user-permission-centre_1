package com.sd365.permission.centre.dao.mapper;


import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.DictionaryType;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface DictionaryTypeMapper extends CommonMapper<DictionaryType> {
}
