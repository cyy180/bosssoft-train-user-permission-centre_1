package com.sd365.permission.centre.dao.mapper;

import com.sd365.common.core.common.dao.CommonMapper;
import com.sd365.permission.centre.entity.Resource;
import com.sd365.permission.centre.pojo.query.ResourceQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ResourceMapper extends CommonMapper<Resource> {
    /**
     * @param: 模糊查询条件
     * 1. 节点名称 name
     * 2. 父亲节点 parentId
     * @return: SubSystemDTOs
     * @see
     * @since
     */
    List<Resource> commonQuery(ResourceQuery resourceQuery);

    Resource findById(Long id);

    Resource findByParentId(Long parentId);

    Boolean updateResource(Resource resource);

    List<Resource> getResourceByRoleId(List<Long> list);


}
