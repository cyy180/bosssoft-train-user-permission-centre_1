package com.sd365.permission.centre.entity;

import com.sd365.common.core.common.pojo.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import javax.persistence.*;

@ApiModel(value="com.sd365.permission.centre.entity.SysLog")
@Table(name = "sys_log")
@Data
public class SysLog extends BaseEntity {
    /**
     * 操作人员
     */
    @ApiModelProperty(value="userName操作人员")
    private String userName;

    /**
     * 工号
     */
    @ApiModelProperty(value="businessTime操作时间")
    private String code;
    /**
     * 操作时间
     */
    @ApiModelProperty(value="businessTime操作时间")
    private Date businessTime;

    /**
     * 100-130 代表登录操作，130-150 代表密码操作，
     */
    @ApiModelProperty(value="action100-130 代表登录操作，130-150 代表密码操作，")
    private String action;

    /**
     * 
     */
    @ApiModelProperty(value="content")
    private String content;

    /**
     * 租户id
     */
    @ApiModelProperty(value="tenantId租户id")
    private Long tenantId;

    /**
     * 组织机构id
     */
    @ApiModelProperty(value="orgId组织机构id")
    private Long orgId;

    /**
     * 公司ID
     */
    @ApiModelProperty(value="companyId公司ID")
    private Long companyId;

    /**
     * 
     */
    @ApiModelProperty(value="creator")
    private String creator;

    /**
     * 
     */
    @ApiModelProperty(value="modifier")
    private String modifier;

    /**
     * 操作人员
     */
    public String getUserName() {
        return userName;
    }

    /**
     * 操作人员
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * 工号
     */
    public String getCode() { return code; }

    /**
     * 工号
     */
    public void setCode(String code) { this.code = code; }

    /**
     * 操作时间
     */
    public Date getBusinessTime() {
        return businessTime;
    }

    /**
     * 操作时间
     */
    public void setBusinessTime(Date businessTime) {
        this.businessTime = businessTime;
    }

    /**
     * 100-130 代表登录操作，130-150 代表密码操作，
     */
    public String getAction() {
        return action;
    }

    /**
     * 100-130 代表登录操作，130-150 代表密码操作，
     */
    public void setAction(String action) {
        this.action = action;
    }

    /**
     * 
     */
    public String getContent() {
        return content;
    }

    /**
     * 
     */
    public void setContent(String content) {
        this.content = content;
    }

    /**
     * 租户id
     */
    public Long getTenantId() {
        return tenantId;
    }

    /**
     * 租户id
     */
    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }

    /**
     * 组织机构id
     */
    public Long getOrgId() {
        return orgId;
    }

    /**
     * 组织机构id
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 公司ID
     */
    public Long getCompanyId() {
        return companyId;
    }

    /**
     * 公司ID
     */
    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    /**
     * 
     */
    public String getCreator() {
        return creator;
    }

    /**
     * 
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * 
     */
    public String getModifier() {
        return modifier;
    }

    /**
     * 
     */
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }
}