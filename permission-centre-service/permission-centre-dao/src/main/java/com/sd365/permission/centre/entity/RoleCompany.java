package com.sd365.permission.centre.entity;

import com.sd365.common.core.common.pojo.entity.TenantBaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;

/**
 * 角色和公司关系实体
 * @author: wujiandong
 * @create: 2022-08-29
 **/
@Data
@ApiModel(value="com.sd365.permission.centre.entity.RoleCompany")
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "basic_role_company")
public class RoleCompany extends TenantBaseEntity {

    /**
     * 由于基类的id使用@Id，且对应表的id字段没有设置自增
     * 使用自定义主键无法插入，所以重新定义，覆盖原有基类id
     */
    private Long id;

    /**
     * 角色ID
     */
    @ApiModelProperty(value="角色ID")
    private Long roleId;

    /**
     * 授权公司ID
     */
    @ApiModelProperty(value="授权公司ID")
    private Long authCompanyId;

}
