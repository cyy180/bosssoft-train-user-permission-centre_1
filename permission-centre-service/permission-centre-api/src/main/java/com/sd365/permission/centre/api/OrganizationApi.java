package com.sd365.permission.centre.api;


import com.sd365.permission.centre.pojo.dto.OrganizationDTO;


import com.sd365.permission.centre.pojo.query.OrganizationQuery;
import com.sd365.permission.centre.pojo.vo.OrganizationStructureVO;
import com.sd365.permission.centre.pojo.vo.OrganizationVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @className: OrganizationApi
 * @description: 组织api
 * @author: 何浪
 * @date: 2020/12/11 17:07
 */
@CrossOrigin
@Api(tags = "机构管理 ",value ="/permission/centre/v1/organization")
@RequestMapping(value = "/permission/centre/v1/organization")
public interface OrganizationApi {

    /**
     * 增加组织
     * @param organizationDTO
     * @return
     */
    @ApiOperation(tags="增加组织",value="")
    @PostMapping(value="")
    @ResponseBody
    Boolean add(@RequestBody @Valid OrganizationDTO organizationDTO);


    /**
     * 删除组织
     * @param id
     * @param version
     * @return
     */
    @ApiOperation(tags="删除组织",value="")
    @DeleteMapping(value="")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required = true) @RequestParam("id") Long id,
                   @ApiParam(value = "当前行版本", required = true) @RequestParam("version") Long version);


    /**
     * 批量删除组织
     * @param organizationDTOS
     * @return
     */
    @ApiOperation(tags="批量删除组织",value="/batch")
    @DeleteMapping(value="/batch")
    @ResponseBody
    Boolean batchRemove(@Valid @RequestBody OrganizationDTO[] organizationDTOS);


    /**
     * 修改组织
     * @param organizationDTO
     * @return
     */
    @ApiOperation(tags="修改组织",value="")
    @PutMapping(value = "")
    @ResponseBody
    Boolean modify(@Valid @RequestBody OrganizationDTO organizationDTO);


    @ApiOperation(tags="批量修改组织",value="")
    @PutMapping(value = "/batch")
    @ResponseBody
    Boolean batchUpdate( @Valid @RequestBody  OrganizationDTO[] organizationDTOS);

    /**
     * @param: 被拷贝的记录id
     * @return: 拷贝后的完整对象
     * @see
     * @since
     */
    @ApiOperation(tags="拷贝组织",value="/copy")
    @PostMapping(value = "/copy")
    @ResponseBody
    OrganizationDTO copy(Long id);

    /**
     *  系统框架GlobalControllerResolver 类分析 参数是否 BaseQuery 类型 ，如果是则 拦截调用
     *  <br> PageHelper 分页方法， 并且将返回的page对象放入TheadLocal ，方法返回参数被 ResponseBodyAware拦截
     *  <br> 其判断 返回值的类型 如果是属于分页的请求则 自动将 List<DriverVO> 装入CommonPage
     *  <br> 并且构建统一应答回去 以上改进优化了 请求和应答的方法的编写
     * @param:
     * @return:
     * @see
     * @since
     */

    @ApiOperation(tags="查询组织",value="")
    @GetMapping(value = "")
    @ResponseBody
    List<OrganizationVO> commonQuery(OrganizationQuery organizationQuery);


    /**
     * @param:
     * @return:
     * @see
     * @since
     */
    @ApiOperation(tags="查询组织通过ID",value="")
    @GetMapping(value = "{id}")
    @ResponseBody
    OrganizationVO queryOrganizationById(@PathVariable("id") Long id);

    /**
     * 获取机构包含的公司以及部门
     * @param id
     * @return
     */

    @ApiOperation(tags="查询组织通过ID/获取全部公司信息",value="")
    @GetMapping(value = "/queryByOrgId")
    @ResponseBody
    List<OrganizationStructureVO> structureVoQueryById( @RequestParam(value = "id",required = false) Long id);
}
