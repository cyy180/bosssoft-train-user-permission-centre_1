package com.sd365.permission.centre.api;

import com.sd365.common.core.annotation.stuffer.CommonFieldStuffer;
import com.sd365.common.core.annotation.stuffer.MethodTypeEnum;
import com.sd365.permission.centre.entity.Module;
import com.sd365.permission.centre.pojo.dto.ModuleDTO;
import com.sd365.permission.centre.pojo.query.IdVersionQuery;
import com.sd365.permission.centre.pojo.query.ModuleQuery;
import com.sd365.permission.centre.pojo.vo.ModuleVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin
@Api(tags = "模块管理",value = "/permission/centre/v1/module")
@RequestMapping(value = "/permission/centre/v1/module")
public interface ModuleApi {

    /**
     * 模块查询
     * @param moduleQuery 封装的模块信息
     * @return 模块VO
     */
    @ApiOperation(tags = "查询模块信息，分页", value = "")
    @GetMapping("")
    @ResponseBody
    @CommonFieldStuffer(methodType = MethodTypeEnum.QUERY)
    List<ModuleVO> commonQuery(ModuleQuery moduleQuery);

    /**
     * 批量更新
     * @param moduleDTOS
     * @return
     */
    @ApiOperation(tags="批量更新",value="/batch")
    @PutMapping(value = "/batch")
    @ResponseBody
    @CommonFieldStuffer(methodType = MethodTypeEnum.UPDATE)
    Boolean batchUpdate(@RequestBody List<ModuleDTO> moduleDTOS);

    /**
     * 添加模块信息
     * @param moduleDTO 模块
     * @return
     */
    @ApiOperation(tags = "增加模块信息", value = "/add")
    @PostMapping(value = "/add")
    @ResponseBody
    Boolean add(@RequestBody @Valid ModuleDTO moduleDTO);

    /**
     * 根据id和版本删除模块
     * @param id \\
     * @param version 版本
     * @return
     */
    @ApiOperation(tags = "删除模块信息", value = "/delete")
    @DeleteMapping(value = "/delete")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required = true) @RequestParam(value = "id", required = true) Long id,
                   @ApiParam(value = "当前行版本", required = true) @RequestParam(value = "version", required = true) Long version);


    /**
     * 批量删除模块
     * @param idVersionQueryList
     * @return
     */
    @ApiOperation(tags = "批量删除模块信息", value = "/batch")
    @DeleteMapping(value = "/batch")
    @ResponseBody
    @CommonFieldStuffer(methodType = MethodTypeEnum.DELETE)
    Boolean removeBatch(@RequestBody List<IdVersionQuery> idVersionQueryList);


    /**
     * 修改模块信息
     * @param moduleDTO
     * @return
     */
    @ApiOperation(tags = "修改模块信息", value = "/modify")
    @PutMapping(value = "/modify")
    @ResponseBody
    Boolean modify(@Valid @RequestBody ModuleDTO moduleDTO);

    /**
     * 通过id查询
     * @param id
     * @return
     */
    @ApiOperation(tags = "通过id查询", value = "")
    @GetMapping(value = "{id}")
    @ResponseBody
    ModuleVO queryModuleById(@PathVariable("id") Long id);

    /**
     * 更新单条启用状态
     * @param moduleDTO
     * @return
     */
    @ApiOperation(tags = "通过id查询", value = "/status")
    @PutMapping(value = "/status")
    @ResponseBody
    Boolean status(@RequestBody  ModuleDTO moduleDTO);
}
