package com.sd365.permission.centre.api;


import com.sd365.permission.centre.pojo.dto.DeletePositionDTO;
import com.sd365.permission.centre.pojo.dto.PositionDTO;
import com.sd365.permission.centre.pojo.query.PositionQuery;
import com.sd365.permission.centre.pojo.vo.PositionVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author yangshaoqi
 * @version 1.0.0
 * @class UserApi
 * @classdesc 用户管理接口 主要是包含增删改查以及对用户分配角色
 * @date 2020.12.14  20点28分
 */
@CrossOrigin
@Api(tags = "职位管理 ", value = "/permission/centre/v1/position")
@RequestMapping(value = "/permission/centre/v1/position")
public interface PositionApi {

    /**
     * @param: 职位DTO
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     * @see
     * @since
     */

    @ApiOperation(tags = "增加职位", value = "")
    @PostMapping(value = "")
    @ResponseBody
    Boolean add(@RequestBody @Valid PositionDTO positionDTO);


    /**
     * @param: 删除用户，若有对应角色则中间表用户-角色也需一并删除
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     * @see
     * @since
     */
    @ApiOperation(tags = "删除职位", value = "")
    @DeleteMapping(value = "")
    @ResponseBody
    Boolean remove(@ApiParam(value = "当前行id", required = true) @RequestParam("id") Long id,
                   @ApiParam(value = "当前行版本", required = true) @RequestParam("version") Long version);


    /**
     * @param: 职位DTO
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     * @see
     * @since
     */
    @ApiOperation(tags = "修改职位", value = "")
    @PutMapping(value = "")
    @ResponseBody
    Boolean modify(@Valid @RequestBody PositionDTO positionDTO);

    /**
     * @param: 职位查询条件
     * @return: 职位VO
     * @see
     * @since
     */

    @ApiOperation(tags = "查询职位", value = "")
    @GetMapping(value = "")
    @ResponseBody
    List<PositionVO> commonQuery(PositionQuery positionQuery);


    /**
     * @param: 职位ID
     * @return: 职位VO
     * @see
     * @since
     */
    @ApiOperation(tags="查询职位通过ID",value="")
    @GetMapping(value = "{id}")
    @ResponseBody
    PositionVO queryPositionById(@PathVariable("id") Long id);




    /**
     * @param: 职位DTO数组
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     * @see
     * @since
     */
    @ApiOperation(tags="批量修改职位",value="")
    @PutMapping(value = "/batchUpdate")
    @ResponseBody
    Boolean batchUpdate(@Valid @RequestBody PositionDTO[] positionDTOS);
    /**
     * @param: 职位DTO数组
     * @return: 成功则true CommonResponse 应答码和消息统一参考基础框架
     * @see
     * @since
     */
    @ApiOperation(tags="批量删除职位",value="")
    @DeleteMapping (value = "/batchDelete")
    @ResponseBody
    Boolean batchDelete(@Valid @RequestBody DeletePositionDTO[] deletePositionDTOS);

}
