package com.sd365.permission.centre.api;

import com.sd365.permission.centre.pojo.query.AuditQuery;
import com.sd365.permission.centre.pojo.vo.AuditVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
/**
 * @Class AuditApi
 * @Description
 * @Author Administrator
 * @Date 2023-02-20  18:35
 * @version 1.0.0
 */
@Validated
@CrossOrigin
@Api(tags = "安全审计", value = "/permission/centre/v1/audit")
@RequestMapping(value = "/permission/centre/v1/audit")
public interface AuditApi {
    /**
     *
     * @param: 审计查询条件
     * @return: 用户VO
     */
    @ApiOperation(tags = "查询审计", value = "")
    @GetMapping(value = "")
    @ResponseBody
    List<AuditVO> commonQuery(@NotNull AuditQuery auditQuery);
    /**
     * @param: 用户ID
     * @return: 用户VO
     * @see
     * @since
     */
    @ApiOperation(tags="查询审计通过id",value="")
    @GetMapping(value = "/{id}")
    @ResponseBody
    AuditVO selectById(@NotNull @PathVariable("id") Long id);

    /**
     * 通过是否使用简单密码查询
     * @param isusp
     * @return
     */
    @ApiOperation(tags="查询审计通过是否使用简单密码",value="")
    @GetMapping(value = "/usp/{isusp}")
    @ResponseBody
    List<AuditVO> selectByUseSimplePwd(@NotNull @PathVariable("isusp") Byte isusp);

    /**
     * 通过是否长期未更新密码查询
     * @param isnup
     * @return
     */
    @ApiOperation(tags="查询审计通过是否长期未修改密码",value="")
    @GetMapping(value = "/nup/{isnup}")
    @ResponseBody
    List<AuditVO> selectByNoUpdatePwd(@NotNull @PathVariable("isnup") Byte isnup);
    /**
     * 通过是否长期未登录查询
     * @param islnl
     * @return
     */
    @ApiOperation(tags="查询审计通过是否长期未登录",value="")
    @GetMapping(value = "/lnl/{islnl}")
    @ResponseBody
    List<AuditVO> selectByLongNoLogin(@NotNull @PathVariable("islnl") Byte islnl);

}
