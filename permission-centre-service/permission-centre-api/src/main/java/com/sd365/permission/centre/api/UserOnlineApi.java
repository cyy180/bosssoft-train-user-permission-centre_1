package com.sd365.permission.centre.api;

import com.sd365.permission.centre.pojo.query.UserOnlineQuery;
import com.sd365.permission.centre.pojo.vo.UserOnlineVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @Description:
 * @Author: WengYu
 * @CreateTime: 2022/06/22 17:47
 */
@CrossOrigin
@Api(tags = "用户在线管理", value ="/permission/centre/v1/user/online")
@RequestMapping(value = "/permission/centre/v1/user/online")
public interface UserOnlineApi {

    @ApiOperation(tags = "查询在线人数，分页", value = "")
    @GetMapping("")
    List<UserOnlineVO> commonQuery(UserOnlineQuery userOnlineQuery);

    /**
     * 强制注销
     * @return
     */
    @ApiOperation(tags = "强制下线", value = "")
    @DeleteMapping("")
    void forceLogout(String ids);

}
