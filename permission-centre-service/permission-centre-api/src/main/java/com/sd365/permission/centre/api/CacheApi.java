/**
 * @Version :1.0
 * @PROJECT_NAME: sd365-permission-centre
 * @PACKAGE_NAME:com.sd365.permission.centre.api
 * @NAME: CacheApi
 * @author:xuandian
 * @DATE: 2022/7/9 15:34
 * @description: 缓存监控
 */
package com.sd365.permission.centre.api;

import com.sd365.permission.centre.pojo.vo.CacheInfoVO;
import com.sd365.permission.centre.pojo.vo.KeyValueVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Map;
/**
 * @Class CacheApi
 * @Description 实现缓存监控管理类的所有接口，该功能主要用于系统的redis值的监控
 * @Author Administrator
 * @Date 2023-02-20  21:48
 * @version 1.0.0
 */
@CrossOrigin
@Api(tags = "缓存监控 ", value = "/permission/centre/v1/monitor")
@RequestMapping(value = "/permission/centre/v1/monitor")
@Validated
public interface CacheApi {
    /**
     * 获取缓存监控Key
     * @param key 非必输入 没有这查询整个
     * @return 返回缓存的汇总性信息
     */
    @ApiOperation(value = "", tags = "缓存监控key...")
    @GetMapping(value = "")
    CacheInfoVO getCacheInfo(@RequestParam(value = "key", required = false) String key);

    /**
     *  获取某一个key的值
     * @param key 缓存key
     * @return 对象值
     */
    @ApiOperation(value = "", tags = "缓存监控value")
    @GetMapping(value = "/queryByKeyCache")
    KeyValueVo queryByKey(@NotNull @RequestParam(value = "key") String key);

    /**
     * 删除缓存key对应的value
     * @param key 缓存key
     * @return true成功false失败
     */
    @ApiOperation(value = "", tags = "删除缓存监控")
    @DeleteMapping(value = "")
    Boolean deleteByKey(@NotNull @RequestParam(value = "key") String key);
}
